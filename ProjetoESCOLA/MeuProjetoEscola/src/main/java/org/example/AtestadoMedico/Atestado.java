package org.example.AtestadoMedico;

import org.example.Usuarios.Aluno;

public class Atestado {
    private Aluno aluno;
    private String nomeArquivo;
    private SStatus status;

    public Atestado() {
    }

    public Atestado(Aluno aluno, String nomeArquivo, SStatus status) {
        this.aluno = aluno;
        this.nomeArquivo = nomeArquivo;
        this.status = status;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

    public SStatus getStatus() {
        return status;
    }

    public void setStatus(SStatus status) {
        this.status = status;
    }
}
