package org.example.Escola;

import org.example.Usuarios.Aluno;
import org.example.Usuarios.CCurso;
import org.example.Usuarios.Professor;
import java.util.ArrayList;
import java.util.List;

public class Disciplina {
    private String nome,descricao;
    private CCurso curso;
    private Professor professor;
    private List<Aluno> aluno;

    public Disciplina() {
    }

    public Disciplina(String nome, String descricao, CCurso curso, Professor professor, List<Aluno> aluno) {
        this.nome = nome;
        this.descricao = descricao;
        this.curso = curso;
        this.professor = professor;
        this.aluno = aluno;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public CCurso getCurso() {
        return curso;
    }

    public void setCurso(CCurso curso) {
        this.curso = curso;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public List<Aluno> getAluno() {
        return aluno;
    }

    public void setAluno(List<Aluno> aluno) {
        this.aluno = aluno;
    }
}
