package org.example.Usuarios;

import org.example.Usuarios.Usuario;

public class Administracao extends Usuario {
    private int idAdmin;
    private String cargo;
    public Administracao(int id, String nome, String email, String login, String senha, int idAdmin, String cargo) {
        super(id, nome, email, login, senha);
        this.idAdmin = idAdmin;
        this.cargo = cargo;
    }

    public int getIdAdmin() {
        return idAdmin;
    }

    public void setIdAdmin(int idAdmin) {
        this.idAdmin = idAdmin;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
}
