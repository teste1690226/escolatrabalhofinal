package org.example.Usuarios;

public class Professor extends Usuario {
    private EEspecializacao especializacao;
    private String disciplina;
    private double valorHoraAula;

    public Professor(int id, String nome, String email, String login, String senha, EEspecializacao especializacao, String disciplina, double valorHoraAula) {
        super(id, nome, email, login, senha);
        this.especializacao = especializacao;
        this.disciplina = disciplina;
        this.valorHoraAula = valorHoraAula;
    }

    public EEspecializacao getEspecializacao() {
        return especializacao;
    }

    public void setEspecializacao(EEspecializacao especializacao) {
        this.especializacao = especializacao;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public double getValorHoraAula() {
        return valorHoraAula;
    }

    public void setValorHoraAula(double valorHoraAula) {
        this.valorHoraAula = valorHoraAula;
    }
}
