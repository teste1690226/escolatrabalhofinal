package org.example.Usuarios;

public class Aluno extends Usuario {
    private String NumeroMatricula;
    private double valorMensalidade;
    private CCurso curso;
    public Aluno(int id, String nome, String email, String login, String senha, String numeroMatricula, double valorMensalidade, CCurso curso) {
        super(id, nome, email, login, senha);
        this.NumeroMatricula = numeroMatricula;
        this.valorMensalidade = valorMensalidade;
        this.curso = curso;
    }

    public String getNumeroMatricula() {
        return NumeroMatricula;
    }

    public void setNumeroMatricula(String numeroMatricula) {
        NumeroMatricula = numeroMatricula;
    }

    public double getValorMensalidade() {
        return valorMensalidade;
    }

    public void setValorMensalidade(double valorMensalidade) {
        this.valorMensalidade = valorMensalidade;
    }

    public CCurso getCurso() {
        return curso;
    }

    public void setCurso(CCurso curso) {
        this.curso = curso;
    }
}
