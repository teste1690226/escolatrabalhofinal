package org.example.Usuarios;

public enum EEspecializacao {

    GRADUACAO, POS_GRADUACAO, MESTRADO, DOUTORADO;
}
